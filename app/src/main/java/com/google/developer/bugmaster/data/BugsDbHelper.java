package com.google.developer.bugmaster.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.developer.bugmaster.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Database helper class to facilitate creating and updating
 * the database from the chosen schema.
 */
public class BugsDbHelper extends SQLiteOpenHelper {
    private static final String TAG = BugsDbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "insects.db";
    private static final int DATABASE_VERSION = 1;

    //Used to read data from res/ and assets/
    private Resources mResources;

    private static final String TABLE_INSECTS = "insects";
    private static final String ID = "id";
    private static final String SCIENTIFIC_NAME = "scientificName";
    private static final String FRIENDLY_NAME = "friendlyName";
    private static final String CLASSIFICATION = "classification";
    private static final String IMAGE_ASSET = "imageAsset";
    private static final String DANGER_LEVEL = "dangerLevel";

    // private static final int ID_IDX = 0;
    private static final int FRIENDLY_NAME_IDX = 1;
    private static final int SCIENTIFIC_NAME_IDX = 2;
    private static final int CLASSIFICATION_IDX = 3;
    private static final int IMAGE_ASSET_IDX = 4;
    private static final int DANGER_LEVEL_IDX = 5;

    private static final String SQL_CREATE_TABLE_INSECTS = "CREATE TABLE " + TABLE_INSECTS + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FRIENDLY_NAME + " TEXT,"
            + SCIENTIFIC_NAME + " TEXT,"
            + CLASSIFICATION + " TEXT,"
            + IMAGE_ASSET + " TEXT,"
            + DANGER_LEVEL + " INTEGER" + ")";

    BugsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mResources = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_INSECTS);
        try {
            readInsectsFromResources(db);
        } catch (IOException e) {
            Log.e(TAG, "Unable to read insects.json");
        } catch (JSONException e) {
            Log.e(TAG, "Unable to parse JSON");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO: Handle database version upgrades
    }

    /**
     * Streams the JSON data from insect.json, parses it, and inserts it into the
     * provided {@link SQLiteDatabase}.
     *
     * @param db Database where objects should be inserted.
     * @throws IOException couldn't open resource
     * @throws JSONException couldn't parse JSON
     */
    private void readInsectsFromResources(SQLiteDatabase db) throws IOException, JSONException {
        StringBuilder builder = new StringBuilder();
        InputStream in = mResources.openRawResource(R.raw.insects);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        fillDatabase(builder.toString(), db);
    }

    private void fillDatabase(String jsonData, SQLiteDatabase db) throws JSONException {
        JSONObject insectsJson = new JSONObject(jsonData);
        JSONArray insectsArray = insectsJson.getJSONArray("insects");
        JSONObject insectObject;
        for (int i = 0; i < insectsArray.length(); i++) {
            insectObject = insectsArray.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(FRIENDLY_NAME, insectObject.getString("friendlyName"));
            values.put(SCIENTIFIC_NAME, insectObject.getString("scientificName"));
            values.put(CLASSIFICATION, insectObject.getString("classification"));
            values.put(IMAGE_ASSET, insectObject.getString("imageAsset"));
            values.put(DANGER_LEVEL, insectObject.getInt("dangerLevel"));
            db.insert(TABLE_INSECTS, null, values);
        }
    }

    public static String getFriendlyName(Cursor cursor) {
        return cursor.getString(FRIENDLY_NAME_IDX);
    }

    public static String getScientificName(Cursor cursor) {
        return cursor.getString(SCIENTIFIC_NAME_IDX);
    }

    public static String getClassification(Cursor cursor) {
        return cursor.getString(CLASSIFICATION_IDX);
    }

    public static String getImageAsset(Cursor cursor) {
        return cursor.getString(IMAGE_ASSET_IDX);
    }

    public static int getDangerLevel(Cursor cursor) {
        return cursor.getInt(DANGER_LEVEL_IDX);
    }

    public String getSortOrder(boolean sortFriendlyName) {
        return sortFriendlyName ? FRIENDLY_NAME + " ASC" : DANGER_LEVEL + " DESC";
    }
}
